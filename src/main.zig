const std = @import("std");
const rl = @import("raylib");

const Pos = struct { x: usize, y: usize };
const Cell = struct {
    value: u8 = 0,
    just_born: bool = true, // spawn last turn
    slide_from: ?Pos = null, // moved last turn
    merge_from: ?Pos = null, // merged last turn

    fn reset_flags(this: *@This()) void {
        this.just_born = false;
        this.slide_from = null;
        this.merge_from = null;
    }

    fn just_merged(this: @This()) bool {
        return this.merge_from != null;
    }

    fn color(cell: @This()) rl.Color {
        return switch (cell.value) {
            1 => rl.GetColor(0xEEE4DAFF), // 2
            2 => rl.GetColor(0xEDE0C8FF), // 4
            3 => rl.GetColor(0xF59563FF), // 8
            4 => rl.GetColor(0xF67C5FFF), // 16
            5 => rl.GetColor(0xF65E3BFF), // 32
            6 => rl.GetColor(0xEDCF72FF), // 64
            7 => rl.GetColor(0xEDCC61FF), // 128
            8 => rl.GetColor(0xEDC850FF), // 256
            9 => rl.GetColor(0xDDB513FF), // 512
            10 => rl.GetColor(0xEDC22EFF), // 1024
            11 => rl.MAGENTA, // 2048
            else => unreachable,
        };
    }

    fn text(cell: @This()) [:0]const u8 {
        return switch (cell.value) {
            1 => "2",
            2 => "4",
            3 => "8",
            4 => "16",
            5 => "32",
            6 => "64",
            7 => "128",
            8 => "256",
            9 => "512",
            10 => "1024",
            11 => "2048",
            else => unreachable,
        };
    }
};

board: [4][4]Cell = .{.{.{}} ** 4} ** 4,
gpa: std.heap.GeneralPurposeAllocator(.{}) = .{},
anim_time_left: f32 = 0,
_rng: std.rand.DefaultPrng = std.rand.DefaultPrng.init(0),

fn rng(this: *@This()) std.rand.Random {
    return this._rng.random();
}

fn spawn(this: *@This()) void {
    var i: u8 = 0;
    loop: for (0..4) |y| for (0..4) |x| {
        defer i += 1;
        const cell = &this.board[x][y];
        if (cell.value != 0) continue;
        if (this.rng().intRangeLessThan(u8, 0, 16) > i) continue;
        cell.value = if (this.rng().boolean()) 1 else 2;
        break :loop;
    };
}

const Dir = enum { u, d, l, r };
fn _merge_or_swap(dest: *Cell, src: *Cell, srcx: usize, srcy: usize) void {
    if (dest.value == 0) {
        // slide
        dest.* = src.*;
        if (dest.slide_from == null)
            dest.slide_from = .{ .x = srcx, .y = srcy };
        src.* = .{};
    } else if (dest.value == src.value and !dest.just_merged() and !src.just_merged()) {
        // merge
        dest.value += 1;
        if (src.slide_from) |slide_from| {
            dest.merge_from = slide_from;
        } else dest.merge_from = .{ .x = srcx, .y = srcy };
        src.* = .{};
    }
}
fn move(this: *@This(), dir: Dir) void {
    switch (dir) {
        .l => {
            for (0..3) |_| for (0..3) |x| for (0..4) |y| {
                const srcx = x + 1;
                const srcy = y;
                _merge_or_swap(&this.board[x][y], &this.board[srcx][srcy], srcx, srcy);
            };
        },
        .r => {
            for (0..3) |_| for (0..3) |m| for (0..4) |y| {
                const x = 3 - m;
                const srcx = x - 1;
                const srcy = y;
                _merge_or_swap(&this.board[x][y], &this.board[srcx][srcy], srcx, srcy);
            };
        },
        .u => {
            for (0..3) |_| for (0..3) |y| for (0..4) |x| {
                const srcx = x;
                const srcy = y + 1;
                _merge_or_swap(&this.board[x][y], &this.board[srcx][srcy], srcx, srcy);
            };
        },
        .d => {
            for (0..3) |_| for (0..3) |m| for (0..4) |x| {
                const y = 3 - m;
                const srcx = x;
                const srcy = y - 1;
                _merge_or_swap(&this.board[x][y], &this.board[srcx][srcy], srcx, srcy);
            };
        },
    }
}
fn move_and_spawn(this: *@This(), dir: Dir) void {
    for (0..4) |y| for (0..4) |x| {
        this.board[x][y].reset_flags();
    };
    this.move(dir);
    this.spawn();
    this.anim_time_left = ANIMATION_DURATION;
}

const ANIMATION_DURATION = 0.2;

fn anim_progress(this: @This()) f32 {
    return this.anim_time_left / ANIMATION_DURATION;
}

// const print = std.fmt.allocPrintZ;
const lerp = std.math.lerp;

const CELLSIZE = 130;
const GAPSIZE = 10;
const FONTSIZE = 40;

pub fn main() !void {
    var this = @This(){};
    this.spawn();
    defer _ = this.gpa.deinit();

    const ga = this.gpa.allocator();

    rl.InitWindow(600, 600, "2048");
    rl.SetTargetFPS(60);

    while (!rl.WindowShouldClose()) {
        var arena = std.heap.ArenaAllocator.init(ga);
        defer arena.deinit();
        const fa = arena.allocator();
        // try print(fa, "{}", .{cell})

        if (this.anim_time_left > 0) {
            this.anim_time_left -= rl.GetFrameTime();
            if (this.anim_time_left < 0) this.anim_time_left = 0;
        }

        if (rl.IsKeyPressed(.KEY_LEFT)) this.move_and_spawn(.l);
        if (rl.IsKeyPressed(.KEY_RIGHT)) this.move_and_spawn(.r);
        if (rl.IsKeyPressed(.KEY_UP)) this.move_and_spawn(.u);
        if (rl.IsKeyPressed(.KEY_DOWN)) this.move_and_spawn(.d);

        const anim_ratio = this.anim_progress();
        var draw_queue = std.ArrayList(DrawCellRequest).init(fa);

        for (0..4) |y| for (0..4) |x| {
            const cell = this.board[x][y];

            var ax: f32 = 0;
            var ay: f32 = 0;
            if (cell.slide_from) |prev| {
                const diffx = @as(f32, @floatFromInt(prev.x)) - @as(f32, @floatFromInt(x));
                const diffy = @as(f32, @floatFromInt(prev.y)) - @as(f32, @floatFromInt(y));

                ax = lerp(0, diffx, anim_ratio) * (CELLSIZE + GAPSIZE);
                ay = lerp(0, diffy, anim_ratio) * (CELLSIZE + GAPSIZE);
            }
            var opacity: f32 = 1;
            if (cell.just_born) opacity = lerp(1, 0, anim_ratio);

            try draw_queue.append(DrawCellRequest.init(cell, x, y, ax, ay, opacity, opacity));

            if (cell.merge_from) |prev| {
                const diffx = @as(f32, @floatFromInt(prev.x)) - @as(f32, @floatFromInt(x));
                const diffy = @as(f32, @floatFromInt(prev.y)) - @as(f32, @floatFromInt(y));

                const ax2 = lerp(0, diffx, anim_ratio) * (CELLSIZE + GAPSIZE);
                const ay2 = lerp(0, diffy, anim_ratio) * (CELLSIZE + GAPSIZE);

                const retro = Cell{ .value = cell.value - 1, .just_born = false, .slide_from = prev };
                const retro_opacity = lerp(0, 1, anim_ratio);
                try draw_queue.append(DrawCellRequest.init(retro, x, y, ax2, ay2, retro_opacity, 1));
            }
        };

        std.sort.pdq(DrawCellRequest, draw_queue.items, void{}, DrawCellRequest.lessThan);

        rl.BeginDrawing();
        defer rl.EndDrawing();

        rl.ClearBackground(rl.RAYWHITE);
        for (0..4) |y| for (0..4) |x| {
            const basex = @as(c_int, @intCast(x)) * (CELLSIZE + GAPSIZE) + 25;
            const basey = @as(c_int, @intCast(y)) * (CELLSIZE + GAPSIZE) + 25;

            rl.DrawRectangleLines(basex, basey, CELLSIZE, CELLSIZE, rl.LIGHTGRAY);
        };
        for (draw_queue.items) |req| {
            drawCell(req);
        }
    }
}

const DrawCellRequest = struct {
    cell: Cell,
    x: usize,
    y: usize,
    ax: f32,
    ay: f32,
    opacity: f32,
    scale: f32,

    fn init(cell: Cell, x: usize, y: usize, ax: f32, ay: f32, opacity: f32, scale: f32) @This() {
        return .{
            .cell = cell,
            .x = x,
            .y = y,
            .ax = ax,
            .ay = ay,
            .opacity = opacity,
            .scale = scale,
        };
    }

    // return true to draw lhs first
    fn lessThan(context: void, lhs: @This(), rhs: @This()) bool {
        _ = context;
        if (lhs.cell.just_born) return false;
        return lhs.cell.value > rhs.cell.value;
    }
};

/// scale: from 0 to 1.
///     0 = smallest, but not 0 pixels
fn drawCell(request: DrawCellRequest) void {
    const cell = request.cell;
    const x = request.x;
    const y = request.y;
    const ax = request.ax;
    const ay = request.ay;
    const opacity = request.opacity;
    const scale = request.scale;

    if (opacity == 0) return;

    const basex = @as(c_int, @intCast(x)) * (CELLSIZE + GAPSIZE) + 25;
    const basey = @as(c_int, @intCast(y)) * (CELLSIZE + GAPSIZE) + 25;

    if (cell.value != 0) {
        const xx = basex + @as(c_int, @intFromFloat(ax));
        const yy = basey + @as(c_int, @intFromFloat(ay));

        var color = cell.color();
        color.a = @intFromFloat(opacity * 255);

        const drift: c_int = @intFromFloat(CELLSIZE * (1 - scale) / 2);

        rl.DrawRectangle(xx + drift, yy + drift, @intFromFloat(CELLSIZE * scale), @intFromFloat(CELLSIZE * scale), color);

        const text = cell.text();
        const textWidth = rl.MeasureText(text, FONTSIZE);
        var text_color = rl.BLACK;
        text_color.a = @intFromFloat(opacity * 255);
        rl.DrawText(text, xx + @divTrunc((CELLSIZE - textWidth), 2), yy + @divTrunc((CELLSIZE - FONTSIZE), 2), FONTSIZE, text_color);
    }
}
